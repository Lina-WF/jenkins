FROM python:3.9
WORKDIR /usr/app/src
COPY index.py ./
ENTRYPOINT ["python", "./index.py"]